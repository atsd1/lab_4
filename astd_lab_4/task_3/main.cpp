#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include <cmath>
using namespace std;

double CalcRPN(vector<string> rpn) {
	stack<string> st = stack<string>();

	for (string item : rpn)
	{
		if (item == "+")
		{
			double b = stod(st.top()); st.pop();
			double a = stod(st.top()); st.pop();
			st.push(to_string(a + b));
		}
		else if (item == "-")
		{
			double b = stod(st.top()); st.pop();
			double a = stod(st.top()); st.pop();
			st.push(to_string(a - b));
		}
		else if (item == "*")
		{
			double b = stod(st.top()); st.pop();
			double a = stod(st.top()); st.pop();
			st.push(to_string(a * b));
		}
		else if (item == "/")
		{
			double b = stod(st.top()); st.pop();
			double a = stod(st.top()); st.pop();
			st.push(to_string(a / b));
		}
		else if (item == "^")
		{
			double b = stod(st.top()); st.pop();
			double a = stod(st.top()); st.pop();
			st.push(to_string(pow(a, b)));
		}
		else if (item == "sqrt")
		{
			double a = stod(st.top()); st.pop();
			st.push(to_string(sqrt(a)));
		}
		else
		{
			st.push(item);
		}
	}

	return stod(st.top());
}

int Priority(string str)
{
	if (str == "+" || str == "-")
	{
		return 1;
	}
	else if (str == "*" || str == "/")
	{
		return 2;
	}
	else if (str == "^")
	{
		return 3;
	}
	else if (str == "sqrt")
	{
		return 4;
	}
	else if (str == "(")
	{
		return 5;
	}
	else
	{
		return 0;
	}
}

bool IsOperation(string str) {
	return (str == "+" || str == "-" || str == "*" || str == "/" || str == "^" || str == "sqrt" || str == "(" || str == ")");
}

vector<string> InfixToRPN(string infix) {
	vector<string> split_infix = vector<string>();

	string temp = "";
	bool (*FunctionPointers[])(char) {
		[](char c) { return (isdigit(c) || c == '.'); },
		[](char c) { return (c == '+' || c == '-' || c == '*' || c == '/' || c == '^'); },
		[](char c) { return (c == '(' || c == ')'); },
		[](char c) { return (bool)isalpha(c); }
	};

	for (int i = 0; i < infix.length(); i++)
	{
		for (bool(*func)(char) : FunctionPointers) {
			if (func(infix[i]))
			{
				temp += infix[i];
				if (!func(infix[i + 1]))
				{
					split_infix.push_back(temp);
					temp = "";
				}
			}
		}
	}

	vector<string> rpn = vector<string>();
	stack<string> st = stack<string>();

	for (string item : split_infix) {
		if (IsOperation(item))
		{
			while (!st.empty() && Priority(st.top()) >= Priority(item))
			{
				if (st.top() == "(")
				{
					st.pop();
					break;
				}
				rpn.push_back(st.top()); st.pop();
			}
			if (item != ")")
			{
				st.push(item);
			}
		}
		else
		{
			rpn.push_back(item);
		}
	}
	while (!st.empty())
	{
		rpn.push_back(st.top()); st.pop();
	}

	return rpn;
}

int main() {
    string infix;

	cout << "Infix: ";
	cin >> infix;

	cout << "RPN: ";
	for (string item : InfixToRPN(infix))
	{
		cout << item + " ";
	}

	cout << "\nAnswer: " << CalcRPN(InfixToRPN(infix)) << endl;
}