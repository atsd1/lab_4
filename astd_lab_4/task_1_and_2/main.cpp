#include <iostream>
#include <vector>
using namespace std;

template<class T>
class LinkedListNode {
public:
    T value;
    LinkedListNode* next;
    LinkedListNode* prev;

    LinkedListNode<T>(T value) : value(value) {}
};

template<class T>
class LinkedList {
public:
    LinkedListNode<T>* begin;

    LinkedList<T>() : begin(nullptr) {}

    void PushFirst(T value) {
        LinkedListNode<T>* node = new LinkedListNode<T>(value);

        if (begin == nullptr)
        {
            begin = node;
            return;
        }

        begin->prev = node;
        node->next = begin;

        begin = node;
    }

    void PushLast(T value) {
        LinkedListNode<T>* node = new LinkedListNode<T>(value);

        if (begin == nullptr)
        {
            begin = node;
            return;
        }

        LinkedListNode<T>* current = begin;

        while (current->next != nullptr)
        {
            current = current->next;
        }

        current->next = node;
        node->prev = current;
    }

    void PushAt(T value, int index) {
        if (index == 0)
        {
            return PushFirst(value);
        }

        LinkedListNode<T>* node = new LinkedListNode<T>(value);
        LinkedListNode<T>* current = begin;

        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        current->prev->next = node;
        node->prev = current->prev;
        node->next = current;
    }

    int Count() {
        int count = 0;
        LinkedListNode<T>* current = begin;

        while (current != nullptr)
        {
            current = current->next;
            count++;
        }
        return count;
    }

    T At(int index) {
        LinkedListNode<T>* current = begin;

        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        return current->value;
    }

    void DeleteFirst() {
        LinkedListNode<T>* current = begin;

        if (begin->next == nullptr)
        {
            begin = nullptr;
            delete current;
            return;
        }

        begin = begin->next;

        current->next = nullptr;
        begin->prev = nullptr;

        delete current;
    }

    void DeleteLast() {
        LinkedListNode<T>* current = begin;

        if (begin->next == nullptr)
        {
            begin = nullptr;
            delete current;
            return;
        }

        while (current->next != nullptr)
        {
            current = current->next;
        }

        current->prev->next = nullptr;
        current->prev = nullptr;

        delete current;
    }

    void DeleteAt(int index) {
        LinkedListNode<T>* current = begin;
        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        current->prev->next = current->next;
        current->next->prev = current->prev;

        delete current;
    }

    void PrintList() {
        LinkedListNode<T>* current = begin;
        while (current != nullptr)
        {
            cout << current->value << endl;
            current = current->next;
        }
    }

    void Clear() {
        while (begin != nullptr)
        {
            DeleteLast();
        }
    }
};

int MenuOptionSelector(vector<string> options) {
    int choice = 0;
    while (choice < 1 || choice > options.size() )
    {
        for (int i = 0; i < options.size(); i++)
        {
            cout << i + 1 << ". " << options[i] << endl;
        }
        cout << "Select[1-" << options.size() << "]: ";
        cin >> choice;
        system("cls");
    }
    return choice;   
}

int main() {
    LinkedList<int> list = LinkedList<int>();

    bool isRunning = true;
    while (isRunning)
    {
        switch (MenuOptionSelector(vector<string> { "Push element", "Delete element", "Print list", "Clear list", "Exit" }))
        {
        case 1: { // Push element
            int val;
            cout << "Value: ";
            cin >> val;

            switch (MenuOptionSelector(vector<string> { "Push first", "Push last", "Push at" }))
            {
            case 1: { // Push first
                list.PushFirst(val);
            } break;
            case 2: { // Push last
                list.PushLast(val);
            } break;
            case 3: { // Push at
                int index;
                cout << "Index: ";
                cin >> index;

                list.PushAt(val, index);
            } break;
            default:
                break;
            }
        } break;
        case 2: { // Delete element
            switch (MenuOptionSelector(vector<string> { "Delete first", "Delete last", "Delete at" }))
            {
            case 1: { // Delete first
                list.DeleteFirst();
            } break;
            case 2: { // Delete last
                list.DeleteLast();
            } break;
            case 3: { // Delete at
                int index;
                cout << "Index: ";
                cin >> index;

                list.DeleteAt(index);
            } break;
            default:
                break;
            }
        } break;
        case 3: { // Print list
            list.PrintList();
            system("pause");
        } break;
        case 4: { // Clear list
            list.Clear();
        } break;
        default: { // Exit
            isRunning = false;
        } break;
        }
        system("cls");
    }
}